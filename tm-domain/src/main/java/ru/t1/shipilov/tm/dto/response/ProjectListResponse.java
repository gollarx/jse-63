package ru.t1.shipilov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;

import java.util.List;

@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    @Getter
    @Setter
    @Nullable
    List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
