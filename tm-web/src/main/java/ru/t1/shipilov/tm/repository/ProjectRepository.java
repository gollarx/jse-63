package ru.t1.shipilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, Project> projects = new LinkedHashMap<>();

    private ProjectRepository() {
    }

    {
        add(new Project("Project 1"));
        add(new Project("Project 2"));
        add(new Project("Project 3"));
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull String id) {
        projects.remove(id);
    }

}
