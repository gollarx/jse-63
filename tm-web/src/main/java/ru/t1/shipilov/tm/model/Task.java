package ru.t1.shipilov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.shipilov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateStart = new Date();

    private Date dateFinish;

    private String projectId;

    public Task(String name) {
        this.name = name;
    }

}
